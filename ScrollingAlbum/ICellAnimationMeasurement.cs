﻿using System;
namespace ScrollingAlbum
{
    public interface ICellAnimationMeasurement
    {
        int AnimatedCellIndex { get; set; }
        AnimatedCellType AnimatedCellType { get; set; }
        nfloat OriginalInset { get; set; }
        nfloat OriginalContentOffset { get; set; }
        void SetOriginalInsetAndContentOffset(nfloat originalInset, nfloat originalContentOffset);
    }
}
