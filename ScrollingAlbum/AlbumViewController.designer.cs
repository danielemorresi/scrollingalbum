// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ScrollingAlbum
{
	[Register ("AlbumViewController")]
	partial class AlbumViewController
	{
		[Outlet]
		public UIKit.UIView assistantMiddleLine { get; set; }

		[Outlet]
		public ScrollingAlbum.CellConfiguratedCollectionView hdCollectionView { get; set; }

		[Outlet]
		public ScrollingAlbum.CellConfiguratedCollectionView thumbnailCollectionView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (assistantMiddleLine != null) {
				assistantMiddleLine.Dispose ();
				assistantMiddleLine = null;
			}

			if (thumbnailCollectionView != null) {
				thumbnailCollectionView.Dispose ();
				thumbnailCollectionView = null;
			}

			if (hdCollectionView != null) {
				hdCollectionView.Dispose ();
				hdCollectionView = null;
			}
		}
	}
}
