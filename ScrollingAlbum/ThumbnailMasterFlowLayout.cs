﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScrollingAlbum
{
    public enum AnimatedCellType
    {
        Folding,
        Unfolding
    }

    public class ThumbnailMasterFlowLayout : UICollectionViewFlowLayout, ICellBasicMeasurement, ICellAnimationMeasurement, IFlowLayoutInvalidateBehavior, IThumbnailFlowLayoutDraggingBehavior
    {
        #region Class fields
        private nfloat UnfoldingCenterOffset = 0;

        private List<CGPoint> NormalCenterPoints;

        public AccordionAnimationManager AccordionAnimationManager;

        public FlowLayoutSyncManager FlowLayoutSyncManager;
        #endregion

        #region Class methods
        public ThumbnailMasterFlowLayout()
        {
            AnimatedCellType = AnimatedCellType.Folding;
            AnimatedCellIndex = 0;
            NormalCenterPoints = new List<CGPoint>();
        }

        private void StartAnimation(AnimatedCellType type)
        {
            AccordionAnimationManager.StartAnimation(CollectionView, type, GetCellFullWidth(GetAnimatedCellIndexPath()), delegate
            {
                OnAnimationUpdate(type);
            });
        }

        private void OnAnimationUpdate(AnimatedCellType type)
        {
            InvalidateLayout();

            SetContentInset();

            if (type == AnimatedCellType.Unfolding)
            {
                SetContentOffset();
            }
        }

        private nfloat GetSymmetricContentInset()
        {
            nfloat symmetricContentInset = (nfloat)(CollectionView.Superview.Frame.Size.Width / 2.0)
                - GetAdjacentSpacingOfAnimatedCell()
                - (GetAnimatedCellSize().Width / 2);
            return symmetricContentInset;
        }

        private nfloat GetCurrentOffset()
        {
            return CollectionView.ContentOffset.X + CollectionView.ContentInset.Left + (GetCellNormalWidthAndSpacing() / 2);
        }

        private NSIndexPath GetAnimatedCellIndexPath()
        {
            return NSIndexPath.FromItemSection(AnimatedCellIndex, 0);
        }

        private CGPoint GetCenterAfterAnimatedCell(NSIndexPath indexPath)
        {
            if (indexPath.Item > GetAnimatedCellIndexPath().Item)
            {
                return new CGPoint(GetAnimatedCellCenter().X
                    + (GetAnimatedCellSize().Width / 2.0)
                    + GetAdjacentSpacingOfAnimatedCell()
                    + (GetCellNormalWidthAndSpacing() * Math.Max(0, indexPath.Item - AnimatedCellIndex - 1))
                    + (GetCellNormalWidth() / 2.0),
                    GetCellMaximumHeight() / 2.0);
            }
            return CGPoint.Empty;
        }

        private CGSize GetAnimatedCellSize()
        {
            CGSize animatedCellSize = new CGSize((GetCellFullWidth(GetAnimatedCellIndexPath()) - GetCellNormalWidth()) * AccordionAnimationManager.GetProgress() + GetCellNormalWidth(), GetCellMaximumHeight());
            return animatedCellSize;
        }

        private CGPoint GetAnimatedCellCenter()
        {
            return new CGPoint((AnimatedCellIndex * GetCellNormalWidthAndSpacing()) + GetAdjacentSpacingOfAnimatedCell() + (GetAnimatedCellSize().Width / 2), GetCellMaximumHeight() / 2);
        }

        private nfloat GetAdjacentSpacingOfAnimatedCell()
        {
            return (GetCellFullSpacing() - GetCellNormalSpacing()) * AccordionAnimationManager.GetProgress() + GetCellNormalSpacing();
        }

        private void SetContentInset()
        {
            if (CollectionView != null)
            {
                CollectionView.ContentInset = new UIEdgeInsets(CollectionView.ContentInset.Top, GetSymmetricContentInset(), CollectionView.ContentInset.Bottom, GetSymmetricContentInset());
            }
        }

        private void SetContentOffset()
        {
            if (AccordionAnimationManager.GetProgress() < 1 &&
                AccordionAnimationManager.GetProgress() > 0)
            {
                var insetOffset = GetSymmetricContentInset() - OriginalInset;
                nfloat cellCenterOffset = 0;
                if (AnimatedCellType == AnimatedCellType.Unfolding)
                {
                    cellCenterOffset = UnfoldingCenterOffset * AccordionAnimationManager.GetProgress();
                }
                CollectionView.ContentOffset = new CGPoint(
                    OriginalContentOffset - insetOffset - cellCenterOffset,
                    CollectionView.ContentOffset.Y
                );
            }
        }
        #endregion

        #region UICollectionViewFlowLayout
        public override CGSize CollectionViewContentSize
        {
            get
            {
                var contentWidth = 2 * GetAdjacentSpacingOfAnimatedCell()
                    + GetAnimatedCellSize().Width
                    + Math.Max(0, (GetCellCount() - 1) * GetCellNormalWidthAndSpacing());
                return new CGSize(contentWidth, GetCellMaximumHeight());
            }
        }

        public override void PrepareLayout()
        {
            if (!ShouldLayoutEverything)
            {
                return;
            }

            NormalCenterPoints = new List<CGPoint>();

            for (var i = 0; i < GetCellCount(); i++)
            {
                var cellCenter = new CGPoint((GetCellNormalWidthAndSpacing() * i) + GetCellNormalSpacing() + (GetCellNormalWidth() / 2), GetCellMaximumHeight() / 2);
                NormalCenterPoints.Add(cellCenter);
            }

            ShouldLayoutEverything = false;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath indexPath)
        {
            var attributes = base.LayoutAttributesForItem(indexPath);
            if (indexPath.Item < AnimatedCellIndex)
            {
                attributes.Size = GetCellNormalSize();
                attributes.Center = NormalCenterPoints[(int)indexPath.Item];
            }
            else if (indexPath.Item > AnimatedCellIndex)
            {
                attributes.Size = GetCellNormalSize();
                attributes.Center = GetCenterAfterAnimatedCell(indexPath);
            }
            else
            {
                attributes.Size = GetAnimatedCellSize();
                attributes.Center = GetAnimatedCellCenter();
            }

            return attributes;
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
        {
            FlowLayoutSyncManager.DidMove(CollectionView, NSIndexPath.FromItemSection(CurrentCellIndex, 0), 0);

            List<UICollectionViewLayoutAttributes> allAttributes = new List<UICollectionViewLayoutAttributes>();
            for (var i = 0; i < GetCellCount(); i++)
            {
                if (rect.Contains(NormalCenterPoints[i]))
                {
                    var indexPath = NSIndexPath.FromItemSection(i, 0);
                    var attributes = LayoutAttributesForItem(indexPath);
                    allAttributes.Add(attributes);
                }
            }

            return allAttributes.ToArray();
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
        {
            return true;
        }

        public override UICollectionViewLayoutInvalidationContext GetInvalidationContextForBoundsChange(CGRect newBounds)
        {
            var context = base.GetInvalidationContextForBoundsChange(newBounds);

            if (newBounds.Size != CollectionView.Bounds.Size)
            {
                ShouldLayoutEverything = true;
            }

            return context;
        }

        public override void InvalidateLayout(UICollectionViewLayoutInvalidationContext context)
        {
            if (context.InvalidateEverything || context.InvalidateDataSourceCounts)
            {
                ShouldLayoutEverything = true;
            }

            base.InvalidateLayout(context);
        }
        #endregion

        #region ICellBasicMeasurement
        public int CurrentCellIndex
        {
            get
            {
                int currentCellIndex = Math.Min(GetCellCount() - 1, (int)(GetCurrentOffset() / GetCellNormalWidthAndSpacing()));
                return currentCellIndex;
            }
        }

        public nfloat GetCellMaximumWidth()
        {
            nfloat cellMaximumWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellMaximumWidth = collectionView.CellMaximumWidth;
            }

            return cellMaximumWidth;
        }

        public nfloat GetCellFullWidth(NSIndexPath indexPath)
        {
            nfloat cellFullWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellFullWidth = collectionView.GetCellSize(indexPath).Width;
            }

            return cellFullWidth;
        }

        public nfloat GetCellFullSpacing()
        {
            nfloat cellFullSpacing = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellFullSpacing = collectionView.CellFullSpacing;
            }

            return cellFullSpacing;
        }

        public nfloat GetCellNormalWidth()
        {
            nfloat cellNormalWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalWidth = collectionView.CellNormalWidth;
            }

            return cellNormalWidth;
        }

        public nfloat GetCellNormalSpacing()
        {
            nfloat cellNormalSpacing = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalSpacing = collectionView.CellNormalSpacing;
            }

            return cellNormalSpacing;
        }

        public nfloat GetCellNormalWidthAndSpacing()
        {
            nfloat cellNormalWidthAndSpacing = GetCellNormalWidth() + GetCellNormalSpacing();

            return cellNormalWidthAndSpacing;
        }

        public CGSize GetCellNormalSize()
        {
            CGSize cellNormalSize = CGSize.Empty;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalSize = new CGSize(collectionView.CellNormalWidth, collectionView.CellHeight);
            }

            return cellNormalSize;
        }

        public nfloat GetCellMaximumHeight()
        {
            nfloat cellMaximumHeight = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellMaximumHeight = collectionView.CellHeight;
            }

            return cellMaximumHeight;
        }

        public int GetCellCount()
        {
            int cellCount = (int)CollectionView.DataSource.GetItemsCount(CollectionView, 0);

            return cellCount;
        }
        #endregion

        #region ICellAnimationMeasurement
        public int AnimatedCellIndex { get; set; }

        public AnimatedCellType AnimatedCellType { get; set; }

        public nfloat OriginalInset { get; set; }

        public nfloat OriginalContentOffset { get; set; }

        public void SetOriginalInsetAndContentOffset(nfloat originalInset, nfloat originalContentOffset)
        {
            OriginalInset = originalInset;
            OriginalContentOffset = originalContentOffset;
            if (AnimatedCellType == AnimatedCellType.Unfolding)
            {
                UnfoldingCenterOffset = OriginalInset + OriginalContentOffset + (GetCellNormalWidthAndSpacing() / 2) - NormalCenterPoints[CurrentCellIndex].X;
            }
        }
        #endregion

        #region IFlowLayoutInvalidateBehavior
        public bool ShouldLayoutEverything { get; set; }
        #endregion

        #region IThumbnailFlowLayoutDraggingBehavior
        public void FoldCurrentCell()
        {
            StartAnimation(AnimatedCellType.Folding);
        }

        public void UnfoldCurrentCell()
        {
            StartAnimation(AnimatedCellType.Unfolding);
        }
        #endregion
    }
}
