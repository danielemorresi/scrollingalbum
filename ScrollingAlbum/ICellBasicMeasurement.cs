﻿using System;
using CoreGraphics;
using Foundation;

namespace ScrollingAlbum
{
    public interface ICellBasicMeasurement
    {
        int CurrentCellIndex { get; }
        nfloat GetCellMaximumWidth();
        nfloat GetCellFullWidth(NSIndexPath indexPath);
        nfloat GetCellFullSpacing();
        nfloat GetCellNormalWidth();
        nfloat GetCellNormalSpacing();
        nfloat GetCellNormalWidthAndSpacing();
        CGSize GetCellNormalSize();
        nfloat GetCellMaximumHeight();
        int GetCellCount();
    }
}
