﻿using System;
using CoreGraphics;
using Foundation;

namespace ScrollingAlbum
{
    public interface ICellPassiveMeasurement
    {
        int PuppetCellIndex { get; set; }
        nfloat PuppetFractionComplete { get; set; }
        nfloat UnitStepOfPuppet { get; }
    }
}
