﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScrollingAlbum
{
    public class ThumbnailSlaveFlowLayout : UICollectionViewFlowLayout, ICellBasicMeasurement, ICellPassiveMeasurement
    {
        #region Class fields
        private List<CGPoint> EstimatedCenterPoints;

        public bool ShouldLayoutEverything = true;
        #endregion

        #region Class methods
        public ThumbnailSlaveFlowLayout()
        {
            EstimatedCenterPoints = new List<CGPoint>();
        }

        private CGPoint GetCenterAfterNextFocusedCell(NSIndexPath indexPath)
        {
            if (indexPath.Item > GetNext(GetCurrentIndexPath()).Item)
            {
                return new CGPoint(GetNextFocusedCellCenter().X
                    + (GetNextFocusedCellSize().Width / 2)
                    + GetRightSpacingOfNextFocusedCell()
                    + (GetCellNormalWidthAndSpacing() * (indexPath.Item - PuppetCellIndex - 2))
                    + (GetCellNormalWidth() / 2),
                    GetNextFocusedCellCenter().Y);
            }

            return CGPoint.Empty;
        }

        private void SetCollectionViewInset()
        {
            var inset = (nfloat)(CollectionView.Superview.Frame.Size.Width / 2.0)
                - GetCellFullSpacing()
                - ((GetFocusedCellSize().Width + GetNextFocusedCellSize().Width - GetCellNormalWidth()) / 2);

            CollectionView.ContentInset = new UIEdgeInsets(
                CollectionView.ContentInset.Top,
                inset,
                CollectionView.ContentInset.Bottom,
                inset
            );
        }

        private NSIndexPath GetCurrentIndexPath()
        {
            return NSIndexPath.FromItemSection(PuppetCellIndex, 0);
        }

        private nfloat GetLeftSpacingOfFocusedCell()
        {
            return (GetCellFullSpacing() - GetCellNormalSpacing()) * (1 - PuppetFractionComplete) + GetCellNormalSpacing();
        }

        private nfloat GetRightSpacingOfNextFocusedCell()
        {
            return GetCellFullSpacing() + GetCellNormalSpacing() - GetLeftSpacingOfFocusedCell();
        }

        private CGSize GetFocusedCellSize()
        {
            if (PuppetFractionComplete < 0)
            {
                return new CGSize(GetCellFullWidth(GetCurrentIndexPath()), GetCellMaximumHeight());
            }
            else
            {
                return new CGSize((GetCellFullWidth(GetCurrentIndexPath()) - GetCellNormalWidth()) * (1 - PuppetFractionComplete) + GetCellNormalWidth(), GetCellMaximumHeight());
            }
        }

        private CGPoint GetFocusedCellCenter()
        {
            if (PuppetFractionComplete < 0)
            {
                return new CGPoint(GetCellFullSpacing() + (GetCellFullWidth(GetCurrentIndexPath()) / 2), GetCellMaximumHeight() / 2);
            }
            else
            {
                return new CGPoint(PuppetCellIndex * GetCellNormalWidthAndSpacing()
                    + GetLeftSpacingOfFocusedCell()
                    + (GetFocusedCellSize().Width / 2),
                    GetCellMaximumHeight() / 2);
            }
        }

        private CGSize GetNextFocusedCellSize()
        {
            var nextFocusedIndexPath = GetNext(GetCurrentIndexPath());
            var nextFocusedCellSize = new CGSize((GetCellFullWidth(nextFocusedIndexPath) - GetCellNormalWidth()) * PuppetFractionComplete + GetCellNormalWidth(), GetCellMaximumHeight());
            return nextFocusedCellSize;
        }

        private CGPoint GetNextFocusedCellCenter()
        {
            var nextFocusedCellCenter = new CGPoint(GetFocusedCellCenter().X
                + (GetFocusedCellSize().Width / 2)
                + (GetNextFocusedCellSize().Width / 2)
                + GetCellFullSpacing(),
                GetCellMaximumHeight() / 2);

            return nextFocusedCellCenter;
        }

        private NSIndexPath GetPrev(NSIndexPath indexPath)
        {
            if (indexPath.Item > 0)
            {
                return NSIndexPath.FromItemSection(indexPath.Item - 1, 0);
            }
            else
            {
                return indexPath;
            }
        }

        private NSIndexPath GetNext(NSIndexPath indexPath)
        {
            return NSIndexPath.FromItemSection(indexPath.Item + 1, 0);
        }


        private nfloat GetCurrentOffset()
        {
            return CollectionView.ContentOffset.X + CollectionView.ContentInset.Left + (GetCellNormalWidthAndSpacing() / 2);
        }
        #endregion

        #region UICollectionViewFlowLayout
        public override CGSize CollectionViewContentSize
        {
            get
            {
                var contentWidth = 2 * GetCellFullSpacing()
                    + GetFocusedCellSize().Width
                    + GetNextFocusedCellSize().Width
                    + Math.Max(0, (GetCellCount() - 2) * GetCellNormalWidthAndSpacing())
                    + GetCellNormalSpacing();
                return new CGSize(contentWidth, GetCellMaximumHeight());
            }
        }

        public override void PrepareLayout()
        {
            if (!ShouldLayoutEverything)
            {
                return;
            }

            EstimatedCenterPoints = new List<CGPoint>();

            for (var i = 0; i < GetCellCount(); i++)
            {
                var cellCenter = new CGPoint((GetCellNormalWidthAndSpacing() * i) + GetCellNormalSpacing() + (GetCellNormalWidth() / 2), GetCellMaximumHeight() / 2);
                EstimatedCenterPoints.Add(cellCenter);
            }

            ShouldLayoutEverything = false;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath indexPath)
        {
            var attributes = base.LayoutAttributesForItem(indexPath);
            if (indexPath.Item < PuppetCellIndex)
            {
                attributes.Size = GetCellNormalSize();
                attributes.Center = EstimatedCenterPoints[(int)indexPath.Item];
            }
            else if (indexPath.Item > PuppetCellIndex + 1)
            {
                attributes.Size = GetCellNormalSize();
                attributes.Center = GetCenterAfterNextFocusedCell(indexPath);
            }
            else if (indexPath.Item == PuppetCellIndex)
            {
                attributes.Size = GetFocusedCellSize();
                attributes.Center = GetFocusedCellCenter();
            }
            else
            {
                attributes.Size = GetNextFocusedCellSize();
                attributes.Center = GetNextFocusedCellCenter();
            }

            return attributes;
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
        {
            SetCollectionViewInset();

            List<UICollectionViewLayoutAttributes> allAttributes = new List<UICollectionViewLayoutAttributes>();

            for (var i = 0; i < GetCellCount(); i++)
            {
                if (rect.Contains(EstimatedCenterPoints[i]))
                {
                    var indexPath = NSIndexPath.FromItemSection(i, 0);
                    var attributes = LayoutAttributesForItem(indexPath);
                    allAttributes.Add(attributes);
                }
            }

            return allAttributes.ToArray();
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
        {
            return true;
        }

        public override UICollectionViewLayoutInvalidationContext GetInvalidationContextForBoundsChange(CGRect newBounds)
        {
            var context = base.GetInvalidationContextForBoundsChange(newBounds);

            if (newBounds.Size != CollectionView.Bounds.Size)
            {
                ShouldLayoutEverything = true;
            }

            return context;
        }

        public override void InvalidateLayout(UICollectionViewLayoutInvalidationContext context)
        {
            if (context.InvalidateEverything || context.InvalidateDataSourceCounts)
            {
                ShouldLayoutEverything = true;
            }

            base.InvalidateLayout(context);
        }
        #endregion

        #region ICellBasicMeasurement
        public int CurrentCellIndex
        {
            get
            {
                int currentCellIndex = Math.Min(GetCellCount() - 1, (int)(GetCurrentOffset() / GetCellNormalWidthAndSpacing()));
                return currentCellIndex;
            }
        }

        public nfloat GetCellMaximumWidth()
        {
            nfloat cellMaximumWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellMaximumWidth = collectionView.CellMaximumWidth;
            }

            return cellMaximumWidth;
        }

        public nfloat GetCellFullWidth(NSIndexPath indexPath)
        {
            nfloat cellFullWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellFullWidth = collectionView.GetCellSize(indexPath).Width;
            }

            return cellFullWidth;
        }

        public nfloat GetCellFullSpacing()
        {
            nfloat cellFullSpacing = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellFullSpacing = collectionView.CellFullSpacing;
            }

            return cellFullSpacing;
        }

        public nfloat GetCellNormalWidth()
        {
            nfloat cellNormalWidth = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalWidth = collectionView.CellNormalWidth;
            }

            return cellNormalWidth;
        }

        public nfloat GetCellNormalSpacing()
        {
            nfloat cellNormalSpacing = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalSpacing = collectionView.CellNormalSpacing;
            }

            return cellNormalSpacing;
        }

        public nfloat GetCellNormalWidthAndSpacing()
        {
            nfloat cellNormalWidthAndSpacing = GetCellNormalWidth() + GetCellNormalSpacing();

            return cellNormalWidthAndSpacing;
        }

        public CGSize GetCellNormalSize()
        {
            CGSize cellNormalSize = CGSize.Empty;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellNormalSize = new CGSize(collectionView.CellNormalWidth, collectionView.CellHeight);
            }

            return cellNormalSize;
        }

        public nfloat GetCellMaximumHeight()
        {
            nfloat cellMaximumHeight = 0;

            if (CollectionView != null && CollectionView is CellConfiguratedCollectionView collectionView)
            {
                cellMaximumHeight = collectionView.CellHeight;
            }

            return cellMaximumHeight;
        }

        public int GetCellCount()
        {
            int cellCount = (int)CollectionView.DataSource.GetItemsCount(CollectionView, 0);

            return cellCount;
        }
        #endregion

        #region ICellPassiveMeasurement
        public int PuppetCellIndex { get; set; }

        public nfloat PuppetFractionComplete { get; set; }

        public nfloat UnitStepOfPuppet
        {
            get
            {
                return GetCellNormalWidth() + GetCellNormalSpacing();

            }
        }
        #endregion
    }
}
