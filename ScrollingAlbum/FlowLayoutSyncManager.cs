﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScrollingAlbum
{
    public class FlowLayoutSyncManager
    {
        #region Class fields
        private UICollectionView MasterCollectionView;

        public List<CellConfiguratedCollectionView> CollectionViews;

        public CellConfiguratedCollectionView ExSlave;
        #endregion

        #region Class methods
        public FlowLayoutSyncManager()
        {
            CollectionViews = new List<CellConfiguratedCollectionView>();
        }

        public void DidMove(UICollectionView collectionView, NSIndexPath indexPath, nfloat fractionComplete)
        {
            if (IsHdMaster())
            {
                SetThumbnailContentOffset(GetSlaveCollectionView(), indexPath, fractionComplete);
            }
            else
            {
                SetHDContentOffset(GetSlaveCollectionView(), indexPath);
            }
        }

        public void Register(UICollectionView collectionView)
        {
            if (collectionView != null && collectionView is CellConfiguratedCollectionView cellConfiguratedCollectionView)
            {
                CollectionViews.Add(cellConfiguratedCollectionView);
            }
        }

        public UICollectionView GetMasterCollectionView()
        {
            return MasterCollectionView;
        }

        public void SetMasterCollectionView(UICollectionView collectionView)
        {
            MasterCollectionView = collectionView;
            if (!IsSlaveNotChanged())
            {
                if (IsHdMaster())
                {
                    SwitchThumbnailToSlave();
                }
                else
                {
                    SwitchThumbnailToMaster();
                }
            }
        }

        private void SwitchThumbnailToSlave()
        {
            if (GetSlaveCollectionView() != null)
            {
                GetSlaveCollectionView().CollectionViewLayout = new ThumbnailSlaveFlowLayout();
            }
        }

        private void SwitchThumbnailToMaster()
        {
            var newLayout = new ThumbnailMasterFlowLayout();
            newLayout.FlowLayoutSyncManager = this;
            newLayout.AccordionAnimationManager = new AccordionAnimationManager();

            if (MasterCollectionView != null && MasterCollectionView.CollectionViewLayout is ICellPassiveMeasurement oldLayout)
            {
                var originalContentOffset = MasterCollectionView.ContentOffset;
                newLayout.AnimatedCellIndex = oldLayout.PuppetCellIndex;
                MasterCollectionView.SetCollectionViewLayout(newLayout, false);
                MasterCollectionView.SetContentOffset(originalContentOffset, false);
            }
        }

        private void SetHDContentOffset(UICollectionView slave, NSIndexPath indexPath)
        {
            if (slave != null && slave.CollectionViewLayout != null && slave.CollectionViewLayout is ICellBasicMeasurement slaveMeasurement)
            {
                var slaveContentOffset = slaveMeasurement.GetCellMaximumWidth() * indexPath.Item;
                slave.SetContentOffset(new CGPoint(slaveContentOffset - slave.ContentInset.Left, 0), false);
            }
        }

        private void SetThumbnailContentOffset(UICollectionView slave, NSIndexPath indexPath, nfloat fractionComplete)
        {
            nfloat slaveContentOffset;
            if (slave != null && slave is CellConfiguratedCollectionView slaveCollectionView)
            {
                var cellSize = slaveCollectionView.GetCellSize(indexPath);

                if (slaveCollectionView.CollectionViewLayout is ICellPassiveMeasurement slaveLayout)
                {
                    if (fractionComplete < 0)
                    {
                        slaveContentOffset = cellSize.Width * fractionComplete;
                    }
                    else
                    {
                        slaveContentOffset = (slaveLayout.UnitStepOfPuppet * indexPath.Item) + fractionComplete;
                        slaveLayout.PuppetCellIndex = (int)indexPath.Item;
                        slaveLayout.PuppetFractionComplete = fractionComplete;
                    }
                    slaveCollectionView.SetContentOffset(new CGPoint(slaveContentOffset - slaveCollectionView.ContentInset.Left, 0), false);
                }
            }
        }

        private bool IsSlaveNotChanged()
        {
            var slave = GetSlaveCollectionView();
            if (slave != ExSlave)
            {
                ExSlave = slave;
                return false;
            }

            return true;
        }

        private bool IsHdMaster()
        {
            if (GetSlaveCollectionView() != null)
            {
                return !(GetSlaveCollectionView().CollectionViewLayout is HDFlowLayout);
            }

            return false;
        }

        private CellConfiguratedCollectionView GetSlaveCollectionView()
        {
            if (MasterCollectionView != null)
            {
                foreach (CellConfiguratedCollectionView view in CollectionViews)
                {
                    if (view != MasterCollectionView)
                    {
                        return view;
                    }
                }
            }

            return null;
        }
        #endregion
    }
}
