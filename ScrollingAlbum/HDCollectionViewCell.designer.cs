// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ScrollingAlbum
{
	[Register ("HDCollectionViewCell")]
	partial class HDCollectionViewCell
	{
		[Outlet]
		public UIKit.UIImageView photoView { get; set; }

		[Outlet]
		public UIKit.NSLayoutConstraint photoViewHeightConstraint { get; set; }

		[Outlet]
		public UIKit.NSLayoutConstraint photoViewWidthConstraint { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (photoView != null) {
				photoView.Dispose ();
				photoView = null;
			}

			if (photoViewHeightConstraint != null) {
				photoViewHeightConstraint.Dispose ();
				photoViewHeightConstraint = null;
			}

			if (photoViewWidthConstraint != null) {
				photoViewWidthConstraint.Dispose ();
				photoViewWidthConstraint = null;
			}
		}
	}
}
