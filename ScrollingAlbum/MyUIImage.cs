﻿using CoreGraphics;
using UIKit;

namespace ScrollingAlbum
{
    public class MyUIImage : UIImage
    {
        public MyUIImage()
        {
        }

        public static UIImage GetStamp(UIImage image, string index)
        {
            var imageView = new UIImageView(image)
            {
                Frame = new CGRect(0, 0, image.Size.Width, image.Size.Height),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            imageView.AddSubview(GetLabelStamp(index, new CGRect(image.Size.Width * 0.375, image.Size.Height * 0.375, image.Size.Width / 4.0, image.Size.Height / 4.0)));

            return GetImageWithImageView(imageView);
        }

        public static UILabel GetLabelStamp(string index, CGRect frame)
        {
            var labelView = new UILabel(frame)
            {
                Font = UIFont.FromName("HelveticaNeue", frame.Size.Width > 200 ? 320 : 32),
                Text = index,
                TextColor = UIColor.Black,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.White
            };

            return labelView;
        }

        public static UIImage GetImageWithImageView(UIImageView imageView)
        {
            UIGraphics.BeginImageContextWithOptions(imageView.Bounds.Size, false, 0);
            imageView.Layer.RenderInContext(UIGraphics.GetCurrentContext());
            var img = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return img ?? new UIImage();
        }
    }
}
