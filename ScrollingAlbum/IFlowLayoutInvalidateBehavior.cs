﻿using System;
namespace ScrollingAlbum
{
    public interface IFlowLayoutInvalidateBehavior
    {
        bool ShouldLayoutEverything { get; set; }
    }
}
