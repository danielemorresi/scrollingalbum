﻿using System;
using System.Collections.Generic;
using System.IO;
using CoreGraphics;
using UIKit;

namespace ScrollingAlbum
{
    public interface IPhotoModel
    {
        int GetNumberOfPhotos();
        string GetPhotoName(int index);
        CGSize GetPhotoSize(int index);
        UIImage GetPhoto(int index, bool debug);
        UIImage GetPhoto(int index);
    }

    struct PhotoCollection : IPhotoModel
    {
        readonly List<string> PhotoNames;
        readonly Dictionary<string, CGSize> PhotoSizes;

        public PhotoCollection(List<string> photos)
        {
            PhotoNames = photos;
            PhotoSizes = new Dictionary<string, CGSize>();
        }

        public int GetNumberOfPhotos()
        {
            return PhotoNames.Count;
        }

        public string GetPhotoName(int index)
        {
            if (index < PhotoNames.Count)
            {
                return PhotoNames[index];
            }
            else
            {
                return null;
            }
        }

        public CGSize GetPhotoSize(int index)
        {
            var name = GetPhotoName(index);
            if (!string.IsNullOrEmpty(name))
            {
                if (!PhotoSizes.ContainsKey(name))
                {
                    PhotoSizes[name] = GetPhoto(index).Size;
                }

                return PhotoSizes[name];
            }
            else
            {
                return CGSize.Empty;
            }
        }

        public UIImage GetPhoto(int index)
        {
            return GetPhoto(index, false);
        }

        public UIImage GetPhoto(int index, bool debug)
        {
            var name = GetPhotoName(index);

            if (name != "")
            {
                if (debug)
                {
                    return MyUIImage.GetStamp(UIImage.FromBundle("Samples/" + name), index.ToString());
                }
                else
                {
                    return UIImage.FromBundle("Samples/" + name);
                }
            }
            else
            {
                return null;
            }
        }
    }

}
