﻿using System;
namespace ScrollingAlbum
{
    public interface IThumbnailFlowLayoutDraggingBehavior
    {
        void FoldCurrentCell();
        void UnfoldCurrentCell();
    }
}
