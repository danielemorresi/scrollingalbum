﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace ScrollingAlbum
{
    using TimerHandler = Action<NSTimer>;

    public class AccordionAnimationManager
    {
        #region Struct fields
        private NSTimer Timer;

        private AnimatedCellType AnimationType = AnimatedCellType.Folding;

        private nfloat AnimationStartTime = 0;

        private nfloat CellLength = 0;

        private readonly double TimeInterval = 0.02;

        public double AnimationMinimumDuration = 0.25;

        public nfloat CellBaseLength = 80;
        #endregion

        #region Class methods
        public AccordionAnimationManager()
        {

        }

        private void SetupCollectionViewForAnimation(UICollectionView collectionView)
        {
            if (collectionView.CollectionViewLayout is ICellBasicMeasurement cellBasicMeasurement &&
                collectionView.CollectionViewLayout is ICellAnimationMeasurement thumbnailMasterFlowLayout)
            {
                thumbnailMasterFlowLayout.AnimatedCellType = AnimationType;
                thumbnailMasterFlowLayout.SetOriginalInsetAndContentOffset(collectionView.ContentInset.Left, collectionView.ContentOffset.X);
                thumbnailMasterFlowLayout.AnimatedCellIndex = cellBasicMeasurement.CurrentCellIndex;
            }
        }

        private void StartTimer(TimerHandler onProgress)
        {
            if (Timer != null)
            {
                Timer.Invalidate();
            }

            Timer = NSTimer.CreateScheduledTimer(TimeInterval, true, onProgress);
            if (Timer != null)
            {
                NSRunLoop.Main.AddTimer(Timer, NSRunLoopMode.Common);
            }
        }

        private void EndTimer()
        {
            if (Timer != null)
            {
                Timer.Invalidate();
            }
        }

        private double GetAnimationDuration()
        {
            return Math.Max(AnimationMinimumDuration, CellLength / CellBaseLength * AnimationMinimumDuration);
        }

        private nfloat UnfoldingAnimationProgress(double currentTime)
        {
            if (AnimationStartTime == 0)
            {
                return 0;
            }
            else if (currentTime >= AnimationStartTime + GetAnimationDuration())
            {
                EndTimer();
                return 1;
            }
            else
            {
                return (nfloat)((currentTime - AnimationStartTime) / GetAnimationDuration());
            }
        }

        private nfloat FoldingAnimationProgress(double currentTime)
        {
            if (AnimationStartTime == 0)
            {
                return 1;
            }
            else if (currentTime >= AnimationStartTime + GetAnimationDuration())
            {
                EndTimer();
                return 0;
            }
            else
            {
                return 1 - (nfloat)((currentTime - AnimationStartTime) / GetAnimationDuration());
            }
        }

        public void StartAnimation(UICollectionView collectionView, AnimatedCellType animationType, nfloat cellLength, TimerHandler onProgress)
        {
            AnimationType = animationType;
            CellLength = cellLength;
            AnimationStartTime = (nfloat)((double)DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000);

            SetupCollectionViewForAnimation(collectionView);

            StartTimer(onProgress);
        }

        public nfloat GetProgress()
        {
            var currentTime = (nfloat)((double)DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000);
            switch (AnimationType)
            {
                case AnimatedCellType.Folding:
                    return FoldingAnimationProgress(currentTime);
                case AnimatedCellType.Unfolding:
                default:
                    return UnfoldingAnimationProgress(currentTime);
            }
        }
        #endregion
    }
}
